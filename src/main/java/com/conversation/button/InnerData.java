package com.conversation.button;

import java.io.Serializable;

public class InnerData implements Serializable 
{
	private static final long serialVersionUID = 5633268247776250460L;
	private Facebook facebook;
	

	public InnerData() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InnerData(Facebook facebook) {
		super();
		this.facebook = facebook;
	}

	public Facebook getFacebook() {
		return facebook;
	}

	public void setFacebook(Facebook facebook) {
		this.facebook = facebook;
	}
}
