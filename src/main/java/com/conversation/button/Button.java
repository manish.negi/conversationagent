package com.conversation.button;

public interface Button 
{
	public InnerData getButtonsYesNo();
	public InnerData getEducationCourse();
	public InnerData getCountry();
	public InnerData getProceedButton();
	public InnerData getGraduation();
	public InnerData getInflationRatebutton();
	public InnerData getRiskButtons();
	
	
	
	
	
}
