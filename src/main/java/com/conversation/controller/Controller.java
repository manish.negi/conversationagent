package com.conversation.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.conversation.Interceptor.AgeIntent;
import com.conversation.Interceptor.AnnualIncomeIntent;
import com.conversation.Interceptor.ChildAgeIntent;
import com.conversation.Interceptor.ChildFutureIntent;
import com.conversation.Interceptor.ChildNameIntent;
import com.conversation.Interceptor.ChildSavingIntent;
import com.conversation.Interceptor.CountryIntent;
import com.conversation.Interceptor.CourseNameIntent;
import com.conversation.Interceptor.ExactAmountIntent;
import com.conversation.Interceptor.GraduationPostGraduationIntent;
import com.conversation.Interceptor.InflationIntent;
import com.conversation.Interceptor.LoanEntitledLifeInsuranceIntent;
import com.conversation.Interceptor.LoanEntitledSettleIntent;
import com.conversation.Interceptor.LoanEntitledTotalAmountIntent;
import com.conversation.Interceptor.MonthlyLevelExpensesIntent;
import com.conversation.Interceptor.ProceedIntent;
import com.conversation.Interceptor.ProtectionAgeIntent;
import com.conversation.Interceptor.ProtectionIntent;
import com.conversation.Interceptor.ProtectionInvestmentIntent;
import com.conversation.Interceptor.ProtectionYesIntent;
import com.conversation.Interceptor.RiskIntent;
import com.conversation.Interceptor.SchoolingIntent;
import com.conversation.Interceptor.ToddlerIntent;
import com.conversation.Interceptor.WelcomeIntent;
import com.conversation.button.Button;
import com.conversation.button.InnerData;
import com.conversation.commons.DataFromJson;
import com.conversation.response.Response;
import com.conversation.response.WebhookResponse;

@RestController
@RequestMapping("/conversationAgent")
public class Controller 
{
	@Autowired
	private Response response;
	@Autowired
	private Button button;
	@Autowired
	private WelcomeIntent welcomeIntent;
	@Autowired
	private DataFromJson dataFromJson;
	@Autowired
	private AgeIntent ageIntent;
	@Autowired
	private  ChildFutureIntent childFutureIntent;
	@Autowired
	private ChildNameIntent childNameIntent;
	@Autowired
	private SchoolingIntent schoolingIntent;
	@Autowired
	private ChildAgeIntent childAgeIntent;
	@Autowired
	private ToddlerIntent toddlerIntent;
	@Autowired
	private ExactAmountIntent exactAmountIntent;
	@Autowired
	private GraduationPostGraduationIntent graduation;
	@Autowired
	private CourseNameIntent courseNameIntent;
	@Autowired
	private CountryIntent countryIntent;
	@Autowired 
	private ChildSavingIntent childSavingIntent;
	@Autowired
	private AnnualIncomeIntent annualIncomeIntent;
	@Autowired
	private RiskIntent riskIntent;
	@Autowired 
	private ProtectionIntent protectionIntent;
	@Autowired
	private ProtectionYesIntent protectionYesIntent;
	@Autowired
	private ProtectionAgeIntent protectionAgeIntent;
	@Autowired
	private MonthlyLevelExpensesIntent monthlyLevelExpensesIntent;
	@Autowired
	private LoanEntitledSettleIntent loanEntitledSettleIntent;
	@Autowired
	private ProtectionInvestmentIntent protectionInvestmentIntent;
	@Autowired
	private LoanEntitledTotalAmountIntent loanEntitledTotalAmountIntent;
	@Autowired
	private LoanEntitledLifeInsuranceIntent LoanEntitledLifeInsuranceIntent;
	@Autowired
	private ProceedIntent proceedIntent; 
	@Autowired
	private InflationIntent inflationIntent; 

	Map<String,Map<String,String>> ExternalMap = new ConcurrentHashMap<String,Map<String,String>>();
	Map<String,String> internalMap = new HashMap<String,String>();
	@RequestMapping(method = RequestMethod.POST)
	//public String webhook(@RequestBody String obj) {
	public WebhookResponse webhook(@RequestBody String obj) {
		System.out.println("Inside Controller");
		System.out.println(obj.toString());
		String sessionId = "";
		String action = "";
		String nameVariable="", ageVariable="", child_Name="", eChildage="", Graduation="", course="", country="";
		String speech="", savings="", risk="";
		InnerData innerData = new InnerData();
		try {
			JSONObject object = new JSONObject(obj.toString());
			System.out.println("Request "+obj.toString());
			sessionId = object.get("sessionId")+"";
			System.out.println("SessionId--"+sessionId);
			//action = object.getJSONObject("queryResult").get("action")+"";
			action = object.getJSONObject("result").get("action") + "";
			System.out.println("Action----"+action);
			nameVariable=dataFromJson.nameVariable(object,sessionId, ExternalMap);
			ageVariable=dataFromJson.ageVariable(object,sessionId, ExternalMap);
			child_Name=dataFromJson.child_name_Variable(object, sessionId, ExternalMap);
			eChildage=dataFromJson.EChildage_Variable(object, sessionId, ExternalMap);
			Graduation=dataFromJson.graduationPostGraduation_Variable(object, sessionId, ExternalMap);
			course=dataFromJson.course_Variable(object, sessionId, ExternalMap);
			country=dataFromJson.country_Variable(object, sessionId, ExternalMap);
			savings=dataFromJson.Savings_Variable(object, sessionId, ExternalMap);
			risk=dataFromJson.risk_Variable(object, sessionId, ExternalMap);
			internalMap.put("name", nameVariable);
			internalMap.put("age", ageVariable);
			internalMap.put("child_Name", child_Name);
			internalMap.put("EChildage", eChildage);
			internalMap.put("graduation", Graduation);
			internalMap.put("course", course);
			internalMap.put("country", country);
			internalMap.put("savings", savings);
			internalMap.put("risk", risk);
			ExternalMap.put(sessionId, internalMap);

			switch(action.toUpperCase())
			{
			case "INPUT.WELCOME":
			{
				speech=welcomeIntent.welcomeIntentCall(nameVariable);
			}
			break;
			case "INPUT.CHILD.EDUCATION":
			{
				speech=childFutureIntent.childFutureIntent(ExternalMap, sessionId);
			}
			break;
			case "INPUT.AGE":
			{
				speech=childAgeIntent.childAgeIntent(ExternalMap, sessionId);
			}
			break;
			case "CHILD.AGE":
			{
				speech=schoolingIntent.schoolingIntent(ExternalMap, sessionId);
				innerData = button.getButtonsYesNo();
			}
			break;
			case "CHILD.AGE.CHILDEDUCATION-CUSTOM-CUSTOM-YES":
			case "CHILD.AGE.CHILDEDUCATION-CUSTOM-CUSTOM-NO":
				
			{
				innerData = null;
				speech=toddlerIntent.toddler(ExternalMap, sessionId);
				innerData = button.getGraduation();
			}
			break;
			case "INPUT.GRADUATION":
			{
				innerData = null;
				speech=graduation.GraduationPostGraduation(ExternalMap, sessionId);
				innerData = button.getEducationCourse();
				
			}
			break;
			case "COURSE":
			{
				innerData = null;
				speech=courseNameIntent.courserNameIntent(ExternalMap, sessionId);
				innerData = button.getCountry();
			}
			break;
			case "INPUT.COUNTRY":
			{
				speech=childSavingIntent.ChildSaving(ExternalMap, sessionId);
				innerData = button.getProceedButton();
			}
			break;
			case "INPUT.PROCEED":
			{
				innerData = null;
				speech=proceedIntent.proceed(ExternalMap, sessionId);
				innerData = button.getInflationRatebutton();
			}
			break;
			case "INPUT.INFLATION":
			{
				innerData = null;
				speech=inflationIntent.inflation(ExternalMap, sessionId);
			}
			break;
			case "INFLATIONRATE.INFLATIONRATE-CUSTOM":
			{
				innerData = null;
				speech=annualIncomeIntent.AnnualIncome(ExternalMap, sessionId);
				innerData = button.getRiskButtons();
			}
			break;
			case "INPUT.RISK":
			{
				
				speech=riskIntent.riskRuleEngine(ExternalMap, sessionId);	
			}
			break;
			/*--------------------------------------------------------------------------------*/
			case "CHILDEDUCATION.CHILDEDUCATION-CUSTOM-CHILD-NAME":
			{
				speech=childNameIntent.childNameIntent(ExternalMap, sessionId);
			}
			break;
			case "INPUT.EDUCATION":
			{
				speech=schoolingIntent.schoolingIntent(ExternalMap, sessionId);
			}
			break;
			case "EXACTAMOUNT_INVESTMENTAMOUNT":
			{
				speech=exactAmountIntent.ExactAmountCalculation(ExternalMap, sessionId);
			}
			break;
			case "EXACTAMOUNT_INVESTMENTAMOUNT_CHILDAGE":
			{
				speech=toddlerIntent.toddler(ExternalMap, sessionId);
			}
			break;
			
			
			case "CLOSE":
			{
				ExternalMap.clear();
				internalMap.clear();
				speech="Every this is clear ";
			}
			break;
			/*========================Protection=======================================================================*/
			case "INPUT.PROTECTION":
			{
				speech=protectionIntent.protectionIntent(ExternalMap, sessionId);
			}
			break;
			case "PROTECTION.PROTECTION-YES":
			{
				speech=protectionYesIntent.protectionYesIntent(ExternalMap, sessionId);
						
			}
			break;
			case "PROTECTION.PROTECTION-YES.PROTECTION-YES-CUSTOM-PROTECTION-AGE":
			{
				speech=protectionAgeIntent.protectionAgeIntent(ExternalMap, sessionId);
			}
			break;
			case "PROTECTION.PROTECTION-YES.PROTECTION-YES-CUSTOM-PROTECTION-AGE.PROTECTION-YES-CUSTOM-AGE-CUSTOM":
			{
				speech=monthlyLevelExpensesIntent.monthlyLevelExpenses(ExternalMap, sessionId);
			}
			break;
			case "PROTECTION.PROTECTION-INVESTMENTAMOUNT":
			{
				speech=protectionInvestmentIntent.protectionInvestment(ExternalMap, sessionId);
			}
			break;
			/*case "PROTECTION.PROTECTION-INVESTMENTAMOUNT.PROTECTION-YES-CUSTOM-AGE-INVESTMENTAMOUNT-CUSTOM": 
			{
				
			}
			break;*/
			case "PROTECTION.PROTECTION-LOANENTITLED":
			{
				speech=loanEntitledSettleIntent.loanEntitledSettle(ExternalMap, sessionId);	
			}
			break;
			
			case "PROTECTION.PROTECTION-LOANENTITLED-TOTALAMOUNT":
			{
				speech=loanEntitledTotalAmountIntent.loanEntitledTotalAmount(ExternalMap, sessionId);
			}
			break;
			case "PROTECTION.PROTECTION-LOANENTITLED-LIFEINSURANCE":
			{
				speech=LoanEntitledLifeInsuranceIntent.LoanEntitledLifeInsurance(ExternalMap, sessionId);
			}
			
			default:
			{
				System.out.println("This is default Statement No intent Matched, Please try again");
			}
			}
		}catch(Exception ex)
		{
			System.out.println("Exception Occoured");
		}
		System.out.println("Final Speech--"+speech);
		//return response.googleAssistanceResponse(speech);
		WebhookResponse responseObj = new WebhookResponse(speech, speech, innerData);
		return responseObj;
	}
}


