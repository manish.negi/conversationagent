package com.conversation.buttonImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.conversation.button.Button;
import com.conversation.button.Facebook;
import com.conversation.button.InnerButton;
import com.conversation.button.InnerData;

@Service
public class ButtonImpl implements Button 
{
	List<InnerButton> innerbuttonlist = new ArrayList<InnerButton>();
	Facebook fb = new Facebook();
	InnerData innerData = new InnerData();
	InnerButton button = new InnerButton();
	InnerButton button2 = new InnerButton();
	InnerButton button3 = new InnerButton();
	InnerButton button4 = new InnerButton();
	InnerButton button5 = new InnerButton();
	InnerButton button6 = new InnerButton();
	InnerButton button7 = new InnerButton();
	
	

	@Override
	public InnerData getButtonsYesNo() 
	{
		innerbuttonlist=new ArrayList<InnerButton>();;
		innerData=new InnerData();

		button.setText("Let's Calculate");
		button.setPostback("yes");
		innerbuttonlist.add(button);

		button2.setText("Just tell me rough estimate");
		button2.setPostback("no");
		innerbuttonlist.add(button2);

		fb.setButtons(innerbuttonlist);
		fb.setTitle("MLIChatBot");
		fb.setPlatform("API.AI");
		fb.setType("Chatbot");
		fb.setImageUrl("BOT");
		innerData.setFacebook(fb);

		return innerData;
	}
	@Override
	public InnerData getEducationCourse() 
	{
		innerbuttonlist=new ArrayList<InnerButton>();;
		innerData=new InnerData();

		button.setText("Engineering");
		button.setPostback("engineering");
		innerbuttonlist.add(button);

		button2.setText("Medical");
		button2.setPostback("Medical");
		innerbuttonlist.add(button2);
		
		button3.setText("Fashion");
		button3.setPostback("Fashion");
		innerbuttonlist.add(button3);


		button4.setText("Law");
		button4.setPostback("Law");
		innerbuttonlist.add(button4);
		
		button5.setText("MBA/MD/Med");
		button5.setPostback("MBA");
		innerbuttonlist.add(button5);
		
		button6.setText("LLM");
		button6.setPostback("LLM");
		innerbuttonlist.add(button6);
		
		button7.setText("Phd");
		button7.setPostback("Phd");
		innerbuttonlist.add(button7);

		fb.setButtons(innerbuttonlist);
		fb.setTitle("MLIChatBot");
		fb.setPlatform("API.AI");
		fb.setType("Chatbot");
		fb.setImageUrl("BOT");
		innerData.setFacebook(fb);
		return innerData;
	}
	@Override
	public InnerData getCountry() 
	{
		innerbuttonlist=new ArrayList<InnerButton>();;
		innerData=new InnerData();

		button.setText("India");
		button.setPostback("India");
		innerbuttonlist.add(button);

		button2.setText("US");
		button2.setPostback("US");
		innerbuttonlist.add(button2);
		
		button3.setText("UK");
		button3.setPostback("UK");
		innerbuttonlist.add(button3);


		button4.setText("Australia");
		button4.setPostback("Australia");
		innerbuttonlist.add(button4);
		
		button5.setText("Singapore");
		button5.setPostback("Singapore");
		innerbuttonlist.add(button5);
		
		fb.setButtons(innerbuttonlist);
		fb.setTitle("MLIChatBot");
		fb.setPlatform("API.AI");
		fb.setType("Chatbot");
		fb.setImageUrl("BOT");
		innerData.setFacebook(fb);
		return innerData;
	}
	@Override
	public InnerData getProceedButton() 
	{
		innerbuttonlist=new ArrayList<InnerButton>();;
		innerData=new InnerData();

		button.setText("Let's Proceed");
		button.setPostback("proceed");
		innerbuttonlist.add(button);

		button2.setText("Edit this");
		button2.setPostback("edit");
		innerbuttonlist.add(button2);
		
		fb.setButtons(innerbuttonlist);
		fb.setTitle("MLIChatBot");
		fb.setPlatform("API.AI");
		fb.setType("Chatbot");
		fb.setImageUrl("BOT");
		innerData.setFacebook(fb);
		return innerData;
	}
	@Override
	public InnerData getGraduation() 
	{
		innerbuttonlist=new ArrayList<InnerButton>();;
		innerData=new InnerData();

		button.setText("Graduation");
		button.setPostback("graduation");
		innerbuttonlist.add(button);

		button2.setText("PostGraduation");
		button2.setPostback("PostGraduation");
		innerbuttonlist.add(button2);
		
		fb.setButtons(innerbuttonlist);
		fb.setTitle("MLIChatBot");
		fb.setPlatform("API.AI");
		fb.setType("Chatbot");
		fb.setImageUrl("BOT");
		innerData.setFacebook(fb);
		return innerData;
	}
	@Override
	public InnerData getInflationRatebutton() 
	{
		innerbuttonlist=new ArrayList<InnerButton>();;
		innerData=new InnerData();

		button.setText("Let's Proceed");
		button.setPostback("inflation rate");
		innerbuttonlist.add(button);

		button2.setText("Please Enter revised Rate");
		button2.setPostback("revised rate");
		innerbuttonlist.add(button2);
		
		fb.setButtons(innerbuttonlist);
		fb.setTitle("MLIChatBot");
		fb.setPlatform("API.AI");
		fb.setType("Chatbot");
		fb.setImageUrl("BOT");
		innerData.setFacebook(fb);
		return innerData;
	}
	@Override
	public InnerData getRiskButtons() 
	{
		innerbuttonlist=new ArrayList<InnerButton>();;
		innerData=new InnerData();

		button.setText("High");
		button.setPostback("high");
		innerbuttonlist.add(button);

		button2.setText("Medium");
		button2.setPostback("medium");
		innerbuttonlist.add(button2);
		
		button3.setText("Low");
		button3.setPostback("low");
		innerbuttonlist.add(button3);
		
		fb.setButtons(innerbuttonlist);
		fb.setTitle("MLIChatBot");
		fb.setPlatform("API.AI");
		fb.setType("Chatbot");
		fb.setImageUrl("BOT");
		innerData.setFacebook(fb);
		return innerData;
	}
	
}

