package com.conversation.commons;

import java.util.Map;

import org.json.JSONObject;
import org.springframework.stereotype.Service;

@Service
public class DataFromJson 
{
	String name="", age="", child_Name="", EChildage="", graduation="", course="", country="", savings="", risk="";
	public String nameVariable(JSONObject object, String sessionId, Map<String, Map<String,String>> externalMap)
	{
		if(externalMap.containsKey(sessionId))
		{
			if(externalMap.get(sessionId).get("name").toString()==null && "".equalsIgnoreCase(externalMap.get(sessionId).get("name")+""))
			{
				try {
					name = object.getJSONObject("result").getJSONObject("parameters").get("name")+"";
				} catch (Exception e) {
					name = "";
				}
			}
			else
			{
				name=externalMap.get(sessionId).get("name")+"";
			}
		}
		else
		{
			try {
				name = object.getJSONObject("result").getJSONObject("parameters").get("name")+"";
			} catch (Exception e) {
				name = "";
			}
		}
		return name;
	}
	public String ageVariable(JSONObject object, String sessionId, Map<String, Map<String,String>> externalMap)
	{
		if(externalMap.containsKey(sessionId))
		{
			if(externalMap.get(sessionId).get("age").toString()==null || "".equalsIgnoreCase(externalMap.get(sessionId).get("age")+""))
			{
				try {
					age = object.getJSONObject("result").getJSONObject("parameters").get("age")+"";
				} catch (Exception e) {
					age = "";
				}
			}
			else
			{
				age=externalMap.get(sessionId).get("age")+"";
			}
		}
		else
		{
			try {
				age = object.getJSONObject("result").getJSONObject("parameters").get("age")+"";
			} catch (Exception e) {
				age = "";
			}
		}
		return age;
	}
	public String child_name_Variable(JSONObject object, String sessionId, Map<String, Map<String,String>> externalMap)
	{
		if(externalMap.containsKey(sessionId))
		{
			if(externalMap.get(sessionId).get("child_Name").toString()==null || "".equalsIgnoreCase(externalMap.get(sessionId).get("child_Name")+""))
			{
				try {
					child_Name = object.getJSONObject("result").getJSONObject("parameters").get("childName")+"";
				} catch (Exception e) {
					child_Name = "";
				}
			}
			else
			{
				child_Name=externalMap.get(sessionId).get("child_Name")+"";
			}
		}
		else
		{
			try {
				child_Name = object.getJSONObject("result").getJSONObject("parameters").get("childName")+"";
			} catch (Exception e) {
				child_Name = "";
			}
		}
		return child_Name;
	}
	public String EChildage_Variable(JSONObject object, String sessionId, Map<String, Map<String,String>> externalMap)
	{
		if(externalMap.containsKey(sessionId))
		{
			if(externalMap.get(sessionId).get("EChildage").toString()==null || "".equalsIgnoreCase(externalMap.get(sessionId).get("EChildage")+""))
			{
				try {
					EChildage = object.getJSONObject("result").getJSONObject("parameters").get("EChildage")+"";
				} catch (Exception e) {
					EChildage = "";
				}
			}
			else
			{
				EChildage=externalMap.get(sessionId).get("EChildage")+"";
			}
		}
		else
		{
			try {
				EChildage = object.getJSONObject("result").getJSONObject("parameters").get("EChildage")+"";
			} catch (Exception e) {
				EChildage = "";
			}
		}
		return EChildage;
	}
	public String graduationPostGraduation_Variable(JSONObject object, String sessionId, Map<String, Map<String,String>> externalMap)
	{
		if(externalMap.containsKey(sessionId))
		{
			if(externalMap.get(sessionId).get("graduation").toString()==null || "".equalsIgnoreCase(externalMap.get(sessionId).get("graduation")+""))
			{
				try {
					graduation = object.getJSONObject("result").getJSONObject("parameters").get("graduation")+"";
				} catch (Exception e) {
					graduation = "";
				}
			}
			else
			{
				graduation=externalMap.get(sessionId).get("graduation")+"";
			}
		}
		else
		{
			try {
				graduation = object.getJSONObject("result").getJSONObject("parameters").get("graduation")+"";
			} catch (Exception e) {
				graduation = "";
			}
		}
		return graduation;
	}
	public String course_Variable(JSONObject object, String sessionId, Map<String, Map<String,String>> externalMap)
	{
		if(externalMap.containsKey(sessionId))
		{
			if(externalMap.get(sessionId).get("course").toString()==null || "".equalsIgnoreCase(externalMap.get(sessionId).get("course")+""))
			{
				try {
					course = object.getJSONObject("result").getJSONObject("parameters").get("course")+"";
				} catch (Exception e) {
					course = "";
				}
			}
			else
			{
				course=externalMap.get(sessionId).get("course")+"";
			}
		}
		else
		{
			try {
				course = object.getJSONObject("result").getJSONObject("parameters").get("course")+"";
			} catch (Exception e) {
				course = "";
			}
		}
		return course;
	}
	public String country_Variable(JSONObject object, String sessionId, Map<String, Map<String,String>> externalMap)
	{
		if(externalMap.containsKey(sessionId))
		{
			if(externalMap.get(sessionId).get("country").toString()==null || "".equalsIgnoreCase(externalMap.get(sessionId).get("country")+""))
			{
				try {
					country = object.getJSONObject("result").getJSONObject("parameters").get("country")+"";
				} catch (Exception e) {
					country = "";
				}
			}
			else
			{
				country=externalMap.get(sessionId).get("country")+"";
			}
		}
		else
		{
			try {
				country = object.getJSONObject("result").getJSONObject("parameters").get("country")+"";
			} catch (Exception e) {
				country = "";
			}
		}
		return country;
	}
	public String Savings_Variable(JSONObject object, String sessionId, Map<String, Map<String,String>> externalMap)
	{
		if(externalMap.containsKey(sessionId))
		{
			if(externalMap.get(sessionId).get("savings").toString()==null || "".equalsIgnoreCase(externalMap.get(sessionId).get("savings")+""))
			{
				try {
					savings = object.getJSONObject("result").getJSONObject("parameters").get("savings")+"";
				} catch (Exception e) {
					savings = "";
				}
			}
			else
			{
				savings=externalMap.get(sessionId).get("savings")+"";
			}
		}
		else
		{
			try {
				savings = object.getJSONObject("result").getJSONObject("parameters").get("savings")+"";
			} catch (Exception e) {
				savings = "";
			}
		}
		return savings;
	}
	public String risk_Variable(JSONObject object, String sessionId, Map<String, Map<String,String>> externalMap)
	{
		if(externalMap.containsKey(sessionId))
		{
			if(externalMap.get(sessionId).get("risk").toString()==null || "".equalsIgnoreCase(externalMap.get(sessionId).get("risk")+""))
			{
				try {
					risk = object.getJSONObject("result").getJSONObject("parameters").get("risk")+"";
				} catch (Exception e) {
					risk = "";
				}
			}
			else
			{
				risk=externalMap.get(sessionId).get("risk")+"";
			}
		}
		else
		{
			try {
				risk = object.getJSONObject("result").getJSONObject("parameters").get("risk")+"";
			} catch (Exception e) {
				risk = "";
			}
		}
		return risk;
	}




}
