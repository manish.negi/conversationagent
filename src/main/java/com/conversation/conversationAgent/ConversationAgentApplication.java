package com.conversation.conversationAgent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.conversation"})
public class ConversationAgentApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConversationAgentApplication.class, args);
	}
}
