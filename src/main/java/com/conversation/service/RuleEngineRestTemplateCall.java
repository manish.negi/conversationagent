package com.conversation.service;

import java.util.Map;

public interface RuleEngineRestTemplateCall 
{
	public String RuleEngineRestCall(Map<String,Map<String,String>> map, String sessionId);

}
