package com.conversation.service;

import java.util.Map;

public interface CalculateAmountIntent 
{
	public void CalculateAmount(Map<String,Map<String,String>> map, String sessionId);

}
