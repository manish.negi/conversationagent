package com.conversation.InterceptorImpl;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.conversation.Interceptor.ProtectionAgeIntent;
@Service
public class ProtectionAgeIntentImpl implements ProtectionAgeIntent 
{
	String speech="";
	@Override
	public String protectionAgeIntent(Map<String, Map<String, String>> map, String sessionId) {
		speech="Let us now look at your family expenditure. Could you also share the monthly expenses?";
		return speech;
	}

}
