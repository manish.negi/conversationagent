package com.conversation.InterceptorImpl;

import java.util.Map;

import com.conversation.Interceptor.ProtectionInvestmentAmountIntent;

public class ProtectionInvestmentAmountIntentImpl implements ProtectionInvestmentAmountIntent 
{
	String speech="";
	@Override
	public String protectionInvestmentAmount(Map<String, Map<String, String>> map, String sessionId) {
		speech="Let’s now look at some of your liabilities. Please share how much loan are you entitled to settle?";
		return speech;
	}
	

}
