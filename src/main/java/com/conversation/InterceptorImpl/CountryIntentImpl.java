package com.conversation.InterceptorImpl;

import java.util.Map;
import org.springframework.stereotype.Service;
import com.conversation.Interceptor.CountryIntent;

@Service
public class CountryIntentImpl implements CountryIntent 
{
	String speech="";
	@Override
	public String country(Map<String, Map<String, String>> map, String sessionId) {
		if(map.containsKey(sessionId))
		{
			speech=""+map.get(sessionId).get("course").toString()+" education in "
					+map.get(sessionId).get("country").toString()+ " is a great choice. If you have already "
					+ "started investing for your child’s education, "
					+ "please share the amount you have saved till now "
					+ " MF/FD/Savings a/c, or any other. "
					+ "I will take that into account for our calculation, Please share the amount";
		}
		else{
			speech="Internal Glitch";
		}
		return speech;
	}
}