package com.conversation.InterceptorImpl;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.conversation.Interceptor.ExactAmountIntent;
@Service
public class ExactAmountIntentImpl implements ExactAmountIntent 
{
	String speech="";
	@Override
	public String ExactAmountCalculation(Map<String, Map<String, String>> map, String sessionId) {
		if(map.containsKey(sessionId))
		{
			speech=" To help you arrive at the customized amount, request you to share a few details with us:\r\n" + 
					" Please share current age of " + map.get(sessionId).get("child_Name").toString()+"";
		}
		else{
			speech="Internal Glitch";
		}
		return speech;
	}
}
