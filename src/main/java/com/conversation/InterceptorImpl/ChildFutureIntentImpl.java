package com.conversation.InterceptorImpl;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.conversation.Interceptor.ChildFutureIntent;

@Service
public class ChildFutureIntentImpl implements ChildFutureIntent{
	String speech="";
	@Override
	public String childFutureIntent(Map<String,Map<String,String>> map, String sessionId) 
	{
		System.out.println("Testing:"+map);
		if(map.containsKey(sessionId))
		{
			speech="That’s Great "+map.get(sessionId).get("name").toString()+ " What is the name "
					+ "of your child ?";
		}
		else{
			speech="Internal Glitch";
		}
		return speech;
	}
}

