package com.conversation.InterceptorImpl;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.conversation.Interceptor.LoanEntitledLifeInsuranceIntent;
@Service
public class LoanEntitledLoanInsuranceIntentImpl implements LoanEntitledLifeInsuranceIntent{

	String speech="";
	@Override
	public String LoanEntitledLifeInsurance(Map<String, Map<String, String>> map, String sessionId) {
		speech="Dear "+map.get(sessionId).get("name")+", you will need an additional life cover of <Result> to ensure that your family"
				+ " can continue to lead the present, happy life in the future.\n" + 
				map.get(sessionId).get("name")+", now that you know how much cover you need, "
				+ "lets calculate the right plan for you. I will need just a couple of details from your."
				+ "\n Start the Lead Journey";
				
		return speech;
	}
}
