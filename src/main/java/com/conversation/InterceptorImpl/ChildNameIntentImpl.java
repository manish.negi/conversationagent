package com.conversation.InterceptorImpl;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.conversation.Interceptor.ChildNameIntent;

@Service
public class ChildNameIntentImpl implements ChildNameIntent 
{
	String speech="";
	@Override
	public String childNameIntent(Map<String, Map<String, String>> map, String sessionId) {
		System.out.println("Inside :: ChildNameIntentImpl");
		System.out.println("External Map--"+map);
		if(map.containsKey(sessionId))
		{
			speech=" Cool. Saving for "+map.get(sessionId).get("child_Name").toString()+" future is a wise decision & the best time to start is now ! "
					+ "What would you like to plan for – marriage or education ?";
		}
		else{
			speech="Internal Glitch";
		}
		System.out.println("OutSide :: ChildNameIntentImpl :: speech"+speech);
		return speech;
	}
}

