package com.conversation.InterceptorImpl;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.conversation.Interceptor.ProtectionYesIntent;
@Service
public class ProtectionYesIntentImpl implements ProtectionYesIntent 
{
	String speech="";
	@Override
	public String protectionYesIntent(Map<String, Map<String, String>> map, String sessionId) {
		speech=" We would need some details from you. Please share your age.";
		return speech;
	}

}
