package com.conversation.InterceptorImpl;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.conversation.Interceptor.ProceedIntent;

@Service
public class ProceedIntentImpl implements ProceedIntent 
{
	String speech="";
	@Override
	public String proceed(Map<String, Map<String, String>> map, String sessionId) {
		speech="Inflation is the rate at which these prices increase. I am assuming this to be 8%.";
		return speech;
	}
}
