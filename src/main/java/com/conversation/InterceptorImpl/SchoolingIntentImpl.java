package com.conversation.InterceptorImpl;

import java.util.Map;
import org.springframework.stereotype.Service;
import com.conversation.Interceptor.SchoolingIntent;

@Service
public class SchoolingIntentImpl implements SchoolingIntent{
	String speech="";
	@Override
	public String schoolingIntent(Map<String, Map<String, String>> map, String sessionId) {
		String childAge=map.get(sessionId).get("EChildage");
		if(map.containsKey(sessionId))
		{
			speech=" Cool Planning for education expenses will go a long way in building a successful career for "
					+ map.get(sessionId).get("child_Name").toString()+", "
							+ "Did you know that average cost of post graduation currently is Rs. 15 Lakhs"
					+ " and by 2033 this may rise up to Rs. 48 Lakhs because of steep inflation. "
					/*+ "#Would you want to further calculate exact amount required for "+map.get(sessionId).get("child_Name").toString()+"’s future education? "*/
					+ " would you like to calculate a customized cost of education for "+map.get(sessionId).get("child_Name").toString(); 
		}
		else{
			speech="Internal Glitch";
		}
		return speech;
	}
}
