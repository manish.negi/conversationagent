package com.conversation.InterceptorImpl;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.conversation.Interceptor.InflationIntent;

@Service
public class InflationIntentImpl implements InflationIntent 
{

	String speech="";
	@Override
	public String inflation(Map<String, Map<String, String>> map, String sessionId) 
	{
		speech= " Here’s what you’ve been waiting for When "+map.get(sessionId).get("child_Name").toString()+" "
		+ "turns 18, you will need Rs.5000000 to fund his "
		+ ""+map.get(sessionId).get("course").toString()+" course "
		+ "in "+map.get(sessionId).get("country").toString()+", assuming 8% of inflation \r\n" + 
		" this amount may look steep right now. However, a systematic investment at regular intervals "
		+ "will help you reach this goal. "
		+ "This is where Max Life Child Plans come in picture. "
		+ "These plans help you grow your investments to beat inflation and also protect your child’s "
		+ "future even in your absence. "
		+ "I can recommend the right education "
		+ " solutions that will cater to "+map.get(sessionId).get("child_Name").toString()+" "
		+ " specific needs. For this, I will need just a couple of details from you. \r\n" + 
		"Please share your annual income " + 
		"" ;
		return speech;
	}
}
