package com.conversation.InterceptorImpl;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.conversation.Interceptor.AnnualIncomeIntent;

@Service
public class AnnualIncomeImpl implements AnnualIncomeIntent 
{
	String speech="";
	@Override
	public String AnnualIncome(Map<String, Map<String, String>> map, String sessionId) {
		
		if(map.containsKey(sessionId))
		{
			speech=" Since we need to arrive at the right plan for you, "
					+ "please share the total savings "
					+ "(To arrive at this, deduct your total expenses from your total income. "
					+ "Expenses include loans, household expenses, home rent etc.)"
					+ " what kind of risk you can take.";
		}
		else{
			speech="Internal Glitch";
		}
		return speech;
	}
}
