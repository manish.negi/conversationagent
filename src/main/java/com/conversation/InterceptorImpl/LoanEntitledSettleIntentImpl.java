package com.conversation.InterceptorImpl;

import java.util.Map;
import org.springframework.stereotype.Service;
import com.conversation.Interceptor.LoanEntitledSettleIntent;
@Service
public class LoanEntitledSettleIntentImpl implements LoanEntitledSettleIntent 
{
	String speech="";
	@Override
	public String loanEntitledSettle(Map<String, Map<String, String>> map, String sessionId) {
		speech=" Now, I would need to understand how much money you will require for your future life goals like "
				+ "Child’s education, Marriage, house or retirement. Please enter total amount that you will need for "
				+ "your future goals? Don’t forget to add an amount for each of these goals separately";
		return speech;
	}

}
