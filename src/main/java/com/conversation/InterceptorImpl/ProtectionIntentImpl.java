package com.conversation.InterceptorImpl;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.conversation.Interceptor.ProtectionIntent;
@Service
public class ProtectionIntentImpl implements ProtectionIntent 
{
	String speech="";
	@Override
	public String protectionIntent(Map<String, Map<String, String>> map, String sessionId) {
		speech=" Great choice. Getting a term insurance plan is the bedrock of any individual’s financial planning. "
				+ "Just like the base of a building, this part needs to be the strongest so as to support the entire building."
				+ " "+map.get(sessionId).get("name")+", it is recommended that you take life insurance cover of at least 10 "
				+ "times of your income. This amount will be paid to them in your absence to ensure that they "
				+ "continue living their current lifestyle.\r\n" + 
				""+map.get(sessionId).get("name")+", term insurance needs vary for every individual. "
				+ "It is dependent on various factors like your income, assets, liabilities etc. "
				+ "Would you want to calculate the exact life cover amount required to sufficiently protect your family? (Yes/No) ";
		return speech;
	}

}
