package com.conversation.InterceptorImpl;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.conversation.Interceptor.AgeIntent;

@Service
public class AgeIntentImpl implements AgeIntent{
	String speech="";
	@Override
	public String ageIntent(Map<String,Map<String,String>> map, String sessionId) 
	{
		if(map.containsKey(sessionId))
		{
			speech=" So, "+map.get(sessionId).get("name").toString()+ " Tell me which goal would you want to start planning for ? "
					+ "Child Future, Retirement, Wealth Creation, Savings, Protection only";
		}
		else{
			speech="Internal Glitch";
		}
		return speech;
	}
}
