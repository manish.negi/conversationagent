package com.conversation.InterceptorImpl;

import java.util.Map;
import org.springframework.stereotype.Service;
import com.conversation.Interceptor.CourseNameIntent;

@Service
public class CourserNameIntentImpl implements CourseNameIntent 
{
	String speech="";
	@Override
	public String courserNameIntent(Map<String, Map<String, String>> map, String sessionId) {
		if(map.containsKey(sessionId))
		{
			speech=" Please pick a country where you wish "+map.get(sessionId).get("child_Name").toString()+
					" to pursue a career.";
		}
		else{
			speech="Internal Glitch";
		}
		return speech;
	}
}