package com.conversation.InterceptorImpl;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.conversation.Interceptor.MonthlyLevelExpensesIntent;
@Service
public class MonthlyLevelExpensesIntentImpl implements MonthlyLevelExpensesIntent 
{
	String speech="";
	@Override
	public String monthlyLevelExpenses(Map<String, Map<String, String>> map, String sessionId) {
		speech="To arrive at the right cover, we would also need to take your assets into account. "
				+ "This includes money that you have invested in instruments like ULIP, Life Insurance, "
				+ "Mutual Funds, Savings account, FDs etc. Please mention your total savings and investment amount?";
		return speech;
	}

}
