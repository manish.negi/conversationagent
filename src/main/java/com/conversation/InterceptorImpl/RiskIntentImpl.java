package com.conversation.InterceptorImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.conversation.Interceptor.RiskIntent;
import com.conversation.service.RuleEngineRestTemplateCall;
@Service
public class RiskIntentImpl implements RiskIntent
{
	@Autowired
	RuleEngineRestTemplateCall ruleEngineTemplateCall;
	String speech="";
	List<String>list = new ArrayList<String>();
	StringBuffer sb = new StringBuffer();
	@Override
	public String riskRuleEngine(Map<String, Map<String, String>> map, String sessionId) {
		String ruleResult = ruleEngineTemplateCall.RuleEngineRestCall(map, sessionId);
		String resultArray [] = ruleResult.split(",");
		for(int i=0; i<resultArray.length; i++)
		{
			String Recommendation=resultArray[i];
			list.add(Recommendation);
		}
		for(String value : list)
		{
			System.out.println(value);
			sb.append(value+", ");
		}
		String str = sb.toString();
		String appendValue=str.substring(0, str.length()-2);
		System.out.println("AppendValue---"+appendValue);
		speech=" Basis your profile and your child’s needs, the product that will match your requirements is – "+appendValue+""
				+ ".\n   Hi "+map.get(sessionId).get("name")+" would you like to know more about "+appendValue+" "
				+ " I can arrange for a call with an expert. "
				+ " Please share your contact number.";
		return speech;
	}

}
