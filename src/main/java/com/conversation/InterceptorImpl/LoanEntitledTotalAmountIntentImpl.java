package com.conversation.InterceptorImpl;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.conversation.Interceptor.LoanEntitledTotalAmountIntent;
@Service
public class LoanEntitledTotalAmountIntentImpl implements LoanEntitledTotalAmountIntent{

	String speech="";
	@Override
	public String loanEntitledTotalAmount(Map<String, Map<String, String>> map, String sessionId) {
		speech="Last, but not the least, how much life insurance do you have?";
		return speech;
	}
}
