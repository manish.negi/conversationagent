package com.conversation.InterceptorImpl;

import java.util.Map;
import org.springframework.stereotype.Service;
import com.conversation.Interceptor.ChildAgeIntent;

@Service
public class ChildAgeIntentImpl implements ChildAgeIntent
{
	String speech="";
	@Override
	public String childAgeIntent(Map<String, Map<String, String>> map, String sessionId) {
		if(map.containsKey(sessionId))
		{
			speech="Oh! Nice "+map.get(sessionId).get("name").toString()+" How old is your child ? ";
		}
		else{
			speech="Internal Glitch";
		}
		return speech;
	}
}
