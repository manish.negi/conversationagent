package com.conversation.InterceptorImpl;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.conversation.Interceptor.AmountEditableIntent;

@Service
public class AmountEditableIntentImpl implements AmountEditableIntent 
{
	String speech="";
	@Override
	public String amountEditable(Map<String, Map<String, String>> map, String sessionId) {
		if(map.containsKey(sessionId))
		{
			speech=" Inflation is the rate at which these prices increase. "
					+ "I am assuming this to be 8%. You can edit this too(Editable)";
		}	
		else{
			speech="Internal Glitch";
		}
		return speech;
	}
}