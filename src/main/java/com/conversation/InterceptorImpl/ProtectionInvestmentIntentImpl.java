package com.conversation.InterceptorImpl;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.conversation.Interceptor.ProtectionInvestmentIntent;

@Service
public class ProtectionInvestmentIntentImpl implements ProtectionInvestmentIntent{

	String speech="";
	@Override
	public String protectionInvestment(Map<String, Map<String, String>> map, String sessionId) {
		speech="Let’s now look at some of your liabilities. Please share how much loan are you entitled to settle?";
		return speech;
	}
}
