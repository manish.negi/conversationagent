package com.conversation.InterceptorImpl;
import java.math.BigDecimal;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.conversation.Interceptor.ChildSavingIntent;
import com.conversation.service.CalculateAmountIntent;

@Service
public class ChildSavingIntentImpl implements ChildSavingIntent 
{
	@Autowired
	private CalculateAmountIntent calculateAmountIntent;

	String speech="", age1="";
	@Override
	public String ChildSaving(Map<String, Map<String, String>> map, String sessionId) 
	{
		int durationtoGoal=0, childAge=0, courseFeemod=0;
		String EChildage="";
		if(map.containsKey(sessionId))
		{
			String graduationIdentifier=map.get(sessionId).get("graduation")+"";
			
			EChildage=map.get(sessionId).get("EChildage")+"";
			try
			{
				if("graduation".equalsIgnoreCase(graduationIdentifier)|| "under graduation".equalsIgnoreCase(graduationIdentifier))
				{
					age1="18";
					childAge=Integer.parseInt(EChildage);
					durationtoGoal=18-childAge;
				}
				else
				{
					age1="21";
					durationtoGoal=21-childAge;
				}
			}
			catch(Exception ex)
			{
				System.out.println("Exception occoured");
			}
			try {
			calculateAmountIntent.CalculateAmount(map, sessionId);
			}catch(Exception ex)
			{
				System.out.println("Exception Occoured");
			}
			try {
				 courseFeemod=Integer.parseInt(map.get(sessionId).get("courseFee")+"");
			}
			catch(Exception ex)
			{
				System.out.println("Exception Occoured");
			}
			double value= Math.pow(1.09, durationtoGoal);
			double finalValue=courseFeemod*value;
			String bigDecimalFinalValue=String.format("%.2f", finalValue); //new BigDecimal(finalValue).toPlainString();
			/*speech=" Assuming your child would be requiring this amount at age "
					+ graduationIdentifier+ " "+ bigDecimalFinalValue
					+ " Inflation is the rate at which these prices increase. I am assuming this to be 8%."
					+ " Here’s what you’ve been waiting for – When "+map.get(sessionId).get("child_Name").toString()+" "
					+ "turns "+age1+", you will need Rs."+bigDecimalFinalValue+" to fund his "
					+ ""+map.get(sessionId).get("course").toString()+" course "
					+ "in "+map.get(sessionId).get("country").toString()+", assuming 8% of inflation \r\n" + 
					" this amount may look steep right now. However, a systematic investment at regular intervals "
					+ "will help you reach this goal. "
					+ "This is where Max Life Child Plans come in picture. "
					+ "These plans help you grow your investments to beat inflation and also protect your child’s "
					+ "future even in your absence. "
					+ "I can recommend the right education "
					+ " solutions that will cater to "+map.get(sessionId).get("child_Name").toString()+" "
					+ " specific needs. For this, I will need just a couple of details from you. \r\n" + 
					"Please share your annual income " + 
					"" ;*/
			speech=" For calculation i am assuming that "+map.get(sessionId).get("child_Name").toString()+" "
					+ " will be requiring this amount at the age of "
					+ graduationIdentifier;
					/* " Inflation is the rate at which these prices increase. I am assuming this to be 8%."
					+ " Here’s what you’ve been waiting for – When "+map.get(sessionId).get("child_Name").toString()+" "
					+ "turns "+age1+", you will need Rs."+bigDecimalFinalValue+" to fund his "
					+ ""+map.get(sessionId).get("course").toString()+" course "
					+ "in "+map.get(sessionId).get("country").toString()+", assuming 8% of inflation \r\n" + 
					" this amount may look steep right now. However, a systematic investment at regular intervals "
					+ "will help you reach this goal. "
					+ "This is where Max Life Child Plans come in picture. "
					+ "These plans help you grow your investments to beat inflation and also protect your child’s "
					+ "future even in your absence. "
					+ "I can recommend the right education "
					+ " solutions that will cater to "+map.get(sessionId).get("child_Name").toString()+" "
					+ " specific needs. For this, I will need just a couple of details from you. \r\n" + 
					"Please share your annual income " + 
					"" ;*/
		}	
		else{
			speech="Internal Glitch";
		}
		return speech;
	}
}