package com.conversation.InterceptorImpl;

import org.springframework.stereotype.Service;

import com.conversation.Interceptor.WelcomeIntent;

@Service
public class WelcomeIntentImpl implements WelcomeIntent {
	String speech="";
	@Override
	public String welcomeIntentCall(String name) {
	
		speech="Hello " +name+", I am your Life goal assistant brought to you by Max Life. \r\n" + 
				"I will help you identify & plan for your key financial protection goals. So "+name+" Tell me "
						+ "which goal would you want to start planning for Child Education and Protection";
		return speech;
	}

}
