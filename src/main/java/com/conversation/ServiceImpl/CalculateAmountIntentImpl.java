package com.conversation.ServiceImpl;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.conversation.service.CalculateAmountIntent;

@Service
public class CalculateAmountIntentImpl implements CalculateAmountIntent 
{
	String courseFee="";
	@Override
	public void CalculateAmount(Map<String, Map<String, String>> map, String sessionId) 
	{
		String graduationIdentifier=map.get(sessionId).get("graduation")+"";
		String country=map.get(sessionId).get("country")+"";
		String course = map.get(sessionId).get("course")+"";
		
		if("graduation".equalsIgnoreCase(graduationIdentifier))
		{
			if("engineering".equalsIgnoreCase(course))
			{
				if("India".equalsIgnoreCase(country))
				{
					map.get(sessionId).put("courseFee", "1400000");
				}
				else
				{
					map.get(sessionId).put("courseFee", "4000000");
				}
			}
			else if("Medical".equalsIgnoreCase(course))
			{
				if("India".equalsIgnoreCase(country))
				{
					map.get(sessionId).put("courseFee", "5000000");
				}
				else
				{
					map.get(sessionId).put("courseFee", "2Cr");
				}
			}
			else if("Fashion".equalsIgnoreCase(course))
			{
				if("India".equalsIgnoreCase(country))
				{
					map.get(sessionId).put("courseFee", "2000000");
				}
				else
				{
					map.get(sessionId).put("courseFee", "3500000");
				}
			}
			else
			{
				if("India".equalsIgnoreCase(country))
				{
					map.get(sessionId).put("courseFee", "900000");
				}
				else
				{
					map.get(sessionId).put("courseFee", "1.10Cr");
				}
			}
				
		}
		else
		{
			if("MBA".equalsIgnoreCase(course)|| "MD".equalsIgnoreCase(course) ||"MED".equalsIgnoreCase(course))
			{
				if("India".equalsIgnoreCase(country))
				{
					map.get(sessionId).put("courseFee", "2000000");
				}
				else
				{
					map.get(sessionId).put("courseFee", "1Cr");
				}
			}
			else if("Mtech".equalsIgnoreCase(course)|| "MS".equalsIgnoreCase(course))
			{
				if("India".equalsIgnoreCase(country))
				{
					map.get(sessionId).put("courseFee", "700000");
				}
				else
				{
					map.get(sessionId).put("courseFee", "4000000");
				}

			}
			else if("LLM".equalsIgnoreCase(course))
			{
				if("India".equalsIgnoreCase(country))
				{
					map.get(sessionId).put("courseFee", "400000");
				}
				else
				{
					map.get(sessionId).put("courseFee", "3500000");
				}
			}
			else
			{
				if("India".equalsIgnoreCase(country))
				{
					map.get(sessionId).put("courseFee", "400000");
				}
				else
				{
					map.get(sessionId).put("courseFee", "1Cr");
				}
			}
		}
	}
}
