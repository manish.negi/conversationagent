package com.conversation.ServiceImpl;
import java.util.Map;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.conversation.service.RuleEngineRestTemplateCall;

@Service
public class RuleEngineRestTemplateCallImpl implements RuleEngineRestTemplateCall 
{
	String url = "https://botskilluat.maxlifeinsurance.com/webservice/REST/ChatBotRuleEngine/ChatBotRuleEngine/getUlipData";
	String ruleResponse="";
	@Override
	public String RuleEngineRestCall(Map<String, Map<String, String>> map, String sessionId) 
	{
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		StringBuilder sb=new StringBuilder();
		sb.append(" 	{	 ");
		sb.append(" 	\"age\": \"35\",	");
		sb.append(" 	  \"term\": \"15\",	");
		sb.append(" 	  \"pocket\": \"150000\",	");
		sb.append(" 	  \"plan\": \"Child Education\"	");
		sb.append(" 	}	 ");
		HttpEntity<String> entity=new HttpEntity<String>(sb.toString(),	headers);
		RestTemplate restTemplate=new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, entity,String.class);
		if(response.getStatusCodeValue() == 200)
		{
			ruleResponse = response.getBody();
			System.out.println("--------"+ruleResponse);

		}
		return ruleResponse;
	}

}
