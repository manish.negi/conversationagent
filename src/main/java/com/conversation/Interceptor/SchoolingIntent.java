package com.conversation.Interceptor;

import java.util.Map;

public interface SchoolingIntent 
{
	public String schoolingIntent(Map<String,Map<String,String>> map, String sessionId);

}
