package com.conversation.Interceptor;

import java.util.Map;

public interface ProtectionAgeIntent 
{
	public String protectionAgeIntent(Map<String,Map<String,String>> map, String sessionId);

}
