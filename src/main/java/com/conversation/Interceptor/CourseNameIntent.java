package com.conversation.Interceptor;

import java.util.Map;

public interface CourseNameIntent 
{
	public String courserNameIntent(Map<String,Map<String,String>> map, String sessionId);

}
