package com.conversation.Interceptor;

import java.util.Map;

public interface ChildNameIntent 
{
	public String childNameIntent(Map<String,Map<String,String>> map, String sessionId);

}
