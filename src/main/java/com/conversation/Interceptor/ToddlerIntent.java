package com.conversation.Interceptor;

import java.util.Map;

public interface ToddlerIntent 
{
	public String toddler(Map<String,Map<String,String>> map, String sessionId);

}
