package com.conversation.Interceptor;

import java.util.Map;

public interface RiskIntent {
	public String riskRuleEngine(Map<String,Map<String,String>> map, String sessionId);
}
