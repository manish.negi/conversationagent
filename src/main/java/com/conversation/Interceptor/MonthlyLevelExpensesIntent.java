package com.conversation.Interceptor;

import java.util.Map;

public interface MonthlyLevelExpensesIntent {
	public String monthlyLevelExpenses(Map<String,Map<String,String>> map, String sessionId);

}
