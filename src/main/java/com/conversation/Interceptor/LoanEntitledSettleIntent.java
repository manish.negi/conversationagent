package com.conversation.Interceptor;

import java.util.Map;

public interface LoanEntitledSettleIntent {
	public String loanEntitledSettle(Map<String,Map<String,String>> map, String sessionId);
}
