package com.conversation.Interceptor;

import java.util.Map;

public interface InflationIntent 
{
	public String inflation(Map<String,Map<String,String>> map, String sessionId);

}
