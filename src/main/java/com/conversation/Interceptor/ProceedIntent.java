package com.conversation.Interceptor;

import java.util.Map;

public interface ProceedIntent 
{
	public String proceed(Map<String,Map<String,String>> map, String sessionId);

}
