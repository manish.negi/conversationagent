package com.conversation.Interceptor;

import java.util.Map;

public interface ProtectionInvestmentIntent {

	public String protectionInvestment(Map<String,Map<String,String>> map, String sessionId);
}
