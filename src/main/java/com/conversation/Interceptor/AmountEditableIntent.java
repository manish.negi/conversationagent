package com.conversation.Interceptor;

import java.util.Map;

public interface AmountEditableIntent {
	public String amountEditable(Map<String,Map<String,String>> map, String sessionId);

}
