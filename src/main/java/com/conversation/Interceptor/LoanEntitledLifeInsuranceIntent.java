package com.conversation.Interceptor;

import java.util.Map;

public interface LoanEntitledLifeInsuranceIntent {

	public String LoanEntitledLifeInsurance(Map<String,Map<String,String>> map, String sessionId);
}
