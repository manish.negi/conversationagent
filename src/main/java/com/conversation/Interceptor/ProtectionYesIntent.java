package com.conversation.Interceptor;

import java.util.Map;

public interface ProtectionYesIntent 
{
	public String protectionYesIntent(Map<String,Map<String,String>> map, String sessionId);

}
