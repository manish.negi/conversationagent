package com.conversation.Interceptor;

import java.util.Map;

public interface ChildAgeIntent 
{
	public String childAgeIntent(Map<String,Map<String,String>> map, String sessionId);

}
