package com.conversation.Interceptor;

import java.util.Map;

public interface ProtectionInvestmentAmountIntent {
	public String protectionInvestmentAmount(Map<String,Map<String,String>> map, String sessionId);

}
