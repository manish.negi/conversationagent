package com.conversation.Interceptor;

import java.util.Map;

public interface ProtectionIntent 
{
	public String protectionIntent(Map<String,Map<String,String>> map, String sessionId);

}
