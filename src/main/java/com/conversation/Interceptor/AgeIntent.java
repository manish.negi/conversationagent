package com.conversation.Interceptor;

import java.util.Map;

public interface AgeIntent {
	public String ageIntent(Map<String,Map<String,String>> map, String sessionId);
}
