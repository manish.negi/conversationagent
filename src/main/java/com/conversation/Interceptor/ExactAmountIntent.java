package com.conversation.Interceptor;

import java.util.Map;

public interface ExactAmountIntent 
{
	public String ExactAmountCalculation(Map<String,Map<String,String>> map, String sessionId);

}
