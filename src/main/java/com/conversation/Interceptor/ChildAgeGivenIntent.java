package com.conversation.Interceptor;

import java.util.Map;

public interface ChildAgeGivenIntent 
{
	public String childAgeIntent(Map<String,Map<String,String>> map, String sessionId);

}
