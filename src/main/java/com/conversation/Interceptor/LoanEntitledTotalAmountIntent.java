package com.conversation.Interceptor;

import java.util.Map;

public interface LoanEntitledTotalAmountIntent {

	public String loanEntitledTotalAmount(Map<String,Map<String,String>> map, String sessionId);
}
