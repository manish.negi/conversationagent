package com.conversation.Interceptor;

import java.util.Map;

public interface CountryIntent {
	public String country(Map<String,Map<String,String>> map, String sessionId);

}
