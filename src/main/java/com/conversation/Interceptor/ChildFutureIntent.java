package com.conversation.Interceptor;

import java.util.Map;

public interface ChildFutureIntent 
{
	public String childFutureIntent(Map<String,Map<String,String>> map, String sessionId);

}
